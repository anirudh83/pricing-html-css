const toggle = document.getElementById('toggle');
const flexID = document.getElementById('flexID');
const leftPrice = document.querySelector('#leftPrice')
const centerPrice = document.querySelector('#centerPrice')
const rightPrice = document.querySelector('#rightPrice')

toggle.addEventListener('change', e=>{
        flexID.classList.toggle('show-monthly');
});

leftPrice.addEventListener('click', e=>{
        leftPrice.classList.add('price-box-active');
        if(centerPrice.classList.contains('price-box-active')){
                centerPrice.classList.remove('price-box-active');      
        }
        if(rightPrice.classList.contains('price-box-active'))   {
                rightPrice.classList.remove('price-box-active');   
        }     
});

centerPrice.addEventListener('click', e=>{
        centerPrice.classList.add('price-box-active');
        if(leftPrice.classList.contains('price-box-active')){
                leftPrice.classList.remove('price-box-active');    
        }
        if(rightPrice.classList.contains('price-box-active'))   {
                rightPrice.classList.remove('price-box-active');    
        }       
});

rightPrice.addEventListener('click', e=>{
        rightPrice.classList.add('price-box-active');
        if(leftPrice.classList.contains('price-box-active')){
                leftPrice.classList.remove('price-box-active');    
        }    
        if(centerPrice.classList.contains('price-box-active')){
                centerPrice.classList.remove('price-box-active');      
        }      
});